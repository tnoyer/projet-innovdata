Consignes pour le projet Innov'Data:

=> Symfony 4
=> Des interactions avec une base de données ( Ajout / Modification / Suppression de données)
=> Relation entre les différentes tables de la base de données
=> Ne pas utiliser un projet déjà existant
=> Ne pas y passer plus de 5 H

Bonus  :

=> Utiliser une API

PS : Un code avec des commentaires et des explications sur les fonctions sera valorisé

-------------

Voici les instructions pour installer et lancer l'application web:

1- Aller dans le dossier ou l'on souhaite créer le projet.

2- Cloner le projet depuis gitlab: https://gitlab.com/tnoyer/projet-innovdata

3- Aller dans le projet cloné depuis l'invite de commande

4- Faire un : "composer install" pour installer les dependances

5- Puis faire un: "php bin/console server:run" pour lancer le serveur de symfony

6- On peut ensuite aller sur l'adresse: "http://localhost:8000" pour
verifier que symfony est bien demarrer

7- Dans le fichier .env, modifier la ligne -> DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name

8- Pour créer la base de données, faire un : "php bin/console doctrine:database:create"

9- Pour faire migrer les entités dans la base de données, faire un : composer require doctrine/doctrine-migrations-bundle "^2.0" puis faire un : "php bin/console make:migration" puis un : "php bin/console doctrine:migrations:migrate"

10- Tester les routes /telephone/add (pour ajouter un téléphone dans la base de données) et /telephone/list (pour afficher la liste)
