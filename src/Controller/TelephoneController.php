<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Telephone;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class TelephoneController extends AbstractController
{
  public function telephoneList()
  {
    $repo = $this->getDoctrine()->getRepository(Telephone::class);
    $telephones = $repo->findAll();
    return $this->render('list.html.twig', [
      'telephones' => $telephones,
    ]);
  }

  //fonction pour ajouter un téléphone dans la base de données
  public function telephoneAdd()
  {
    //création d'un objet téléphone
    $tel = new Telephone();
    $tel->setMarque('Nokia');
    $tel->setType('8');
    $tel->setTaille(5.3);

    //enregistrer les données dans la base
    $em = $this->getDoctrine()->getManager();
    $em->persist($tel);
    $em->flush();
    return new Response("Ajout(s) effectué(s) !");
  }

  // public function telephoneNew()
  // {
  //   // création d'un objet téléphone
  //   $tel = new Telephone();
  //
  //   // création d'un formulaire
  //   $form = $this->createFormBuilder($tel)
  //   // Nous précisons aussi le type de champs (TextType::class, NumberType::class)
  //   ->add('marque', TextType::class)
  //   ->add('type', TextType::class)
  //   ->add('taille', Number::class)
  //   //bouton sauvegarde "Création"
  //   ->add('save', SubmitType::class, array('label' => 'Création'))
  //   // getForm permet de créer l'objet formulaire
  //   ->getForm();
  //   // nous récupérons ici les informations du formulaire validée
  //   $form->handleRequest($request);
  //
  //   // Si nous venons de valider le formulaire et s'il est valide (problèmes de type, etc)
  //   if ($form->isSubmitted() && $form->isValid()) {
  //       // nous enregistrons directement l'objet $tel !
  //       // En effet, il a été hydraté grâce au paramètre donné à la méthode createFormBuilder !
  //       $em = $this->getDoctrine()->getManager();
  //       $em->persist($tel);
  //       $em->flush();
  //
  //       return $this->redirectToRoute('telephone_list');
  //   }
  //
  //   return $this->render('add.html.twig', array(
  //     'form' => $form->createView(),
  //   ));
  // }

  public function telephoneUpdate()
  {

  }

  public function telephoneDelete()
  {

  }

}
